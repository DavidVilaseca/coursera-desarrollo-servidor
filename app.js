const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const bicicletasRouter = require('./routes/bicicletas');
const bicicletasAPIRouter = require('./routes/api/bicicletas');
const usuariosAPIRouter = require('./routes/api/usuarios');
const cors = require('cors');
const mongoose = require('mongoose');

let mongoDB = 'mongodb://localhost/red_bicicletas';
mongoose.connect(mongoDB, {useNewUrlParser : true});
mongoose.Promise = global.Promise;
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error: '));


app.use(express.urlencoded({
    extended: true
}))
app.use(express.json())
app.use(cors());
app.use('/bicicletas', bicicletasRouter);
app.use('/api/bicicletas', bicicletasAPIRouter);
app.use('/api/usuarios', usuariosAPIRouter);


app.listen(3000, () => {
    console.log(`Server running on port ${port}.`)
})