let Bicicleta = require("../../models/bicicleta");
const request = require("request");
const mongoose = require("mongoose");
const base_url = "http://localhost:3000/api/bicicletas";

describe("Biccileta API", () => {
    beforeAll(done => {
        const mongoDB = "mongoDB://localhost/testdb";
        mongoose.connect(mongoDB, { useNewUrlParser: true });
        const db = mongoose.connection;
        db.on("error", console.error.bind(console, "conection error"));
        db.once("open", () => {
            console.log("We are conected to test database");
            done();
        });
    });

    afterEach(done => {
        Bicicleta.deleteMany({}, (err, success) => {
            if (err) console.log(err);
            done();
        });
    });

    afterEach((done) => {
        Bicicleta.deleteMany({}, (err, success) => {
            if (err) console.log(err);
            done();
        });
    });

    describe("GET BICICLETAS /", () => {
        it("Status 200", (done) => {
            request.get(base_url, (error, response, body) => {
                let result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);
                done();
            });
        });
    });
    describe("POST BICICLETAS /create", () => {
        it("status 200", (done) => {
            let headers = { "content-type": "application/json" };
            let bici = JSON.stringify({
                                "id": 10,
                                "color": "rojo",
                                "modelo": "urbana",
                                "ubicacion": [0, 0]
                            })
            request.post({
                    headers: headers,
                    url: base_url + "/create",
                    body: bici,
                }, (error, response, body) => {
                    expect(response.statusCode).toBe(200);
                    let bici = JSON.parse(body).bicicleta;
                    console.log(bici);
                    expect(bici.color).toBe("rojo");
                    expect(bici.ubicacion[0]).toBe(0);
                    expect(bici.ubicacion[1]).toBe(0);
                    done();
                }
            );
        });
    });

    describe("DELETE BICICLETAS /delete", () => {});
});

describe("Bicicleta API", () => {
    describe("GET BICICLETAS /", () => {
        it("status 200", () => {
            expect(Bicicleta.allBicis.length).toBe(0);

            let bici = new Bicicleta(1, "rojo", "urbana", [
                0,
                0,
            ]);
            Bicicleta.add(bici);

            request.get("http://localhost:3000/api/bicicletas", (error,response,body) => {
                expect(response.statusCode).toBe(200);
            });
        });
    });
});

// beforeEach(() => {
//     Bicicleta.allBicis = [];
// })

// describe('Bicicleta API', () => {
//     describe('GET BICICLETAS /', () => {
//         it('Status 200', () => {
//             expect(Bicicleta.allBicis.length).toBe(0);
//             const bici = new Bicicleta(1, 'rojo', 'urban', [0, 0]);
//             Bicicleta.add(bici);
//             request.get('http://localhost:3000/api/bicicletas', (error, response, body) => {
//                 expect(response.statusCode).toBe(200);
//             })
//         })
//     });

//     describe('POST BICICLETAS /create', () => {
//         it('STATUS 200', done => {
//             let headers = {'content-type': 'application/json'};
//             let bici = JSON.stringify({
//                 "id": 10,
//                 "color": "rojo",
//                 "modelo": "urbana",
//                 "ubicacion": [0, 0]
//             })
//             request.post({
//                 headers: headers,
//                 url: "http://localhost:3000/api/bicicletas/create",
//                 body: bici
//             }, (error, response, body) => {
//                 expect(response.statusCode).toBe(200);
//                 expect(Bicicleta.findById(10).color).toBe("rojo");
//                 done();
//             })
//         })
//     });

//     describe('DELETE BICICLETAS /delete', () => {
//         it('status 200', done => {
//             let headers = {'content-type': 'application/json'};
//             let bici = JSON.stringify({
//                 "id": 10
//             });
//             request.post({
//                 headers: headers,
//                 url: `http://localhost:3000/api/bicicletas/10/delete`,
//                 body: bici
//             }, (error, response, body) => {
//                 expect(response.statusCode).toBe(200);
//                 let bici = Bicicleta.allBicis.find(el => el.id === 10);
//                 expect(bici).toBe(undefined);
//                 done();
//             })
//         })
//     });

//     describe('UPDATE BICICLETAS /update', () => {
//         it('status 200', done => {
//             let headers = {'content-type': 'application/json'};
//             let bici = JSON.stringify({
//                 "id": 9,
//                 "color": "amarillo",
//                 "modelo": "urbana",
//                 "ubicacion": [0, 0]
//             })
//             request.post({
//                 headers: headers,
//                 url: `http://localhost:3000/api/bicicletas/10/update`,
//                 body: bici
//             }, (error, response, body) => {
//                 expect(response.statusCode).toBe(200);
//                 expect(Bicicleta.findById(10).color).toBe("amarillo");
//                 done();
//             })
//         })
//     });
// })
