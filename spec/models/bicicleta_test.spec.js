const mongoose = require("mongoose");
let Bicicleta = require("../../models/bicicleta");

describe("Testing Bicicletas", () => {
    beforeAll((done) => {
        let mongoDB = "mongodb://localhost/testdb";
        mongoose.connect(mongoDB, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
        });
        const db = mongoose.connection;
        db.on("error", console.error.bind(console, "connection error"));
        db.once("open", () => {
            console.log("We are connected to the test database!");
            done();
        });
    });

    afterEach((done) => {
        Bicicleta.deleteMany({}, (err, success) => {
            if (err) console.log(err);
            done();
        });
    });

    describe("Bicicleta.createInstance", () => {
        it("crea una instancia de Bicicleta", () => {
            var bici = Bicicleta.createInstance(1, "verde", "urbana", [0, 0]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toEqual(0);
            expect(bici.ubicacion[1]).toEqual(0);
        });
    });

    describe("Bicicleta.allBicis", () => {
        it("comienza vacia", (done) => {
            Bicicleta.allBicis(function (err, bicis) {
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe("Bicicleta.add", () => {
        it("agrega solo una bici", (done) => {
            let bici = new Bicicleta({
                code: 1,
                color: "verde",
                modelo: "urbana",
            });
            Bicicleta.add(bici, (err, newBici) => {
                if (err) console.log(err);
                Bicicleta.allBicis((err, bicis) => {
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(bici.code);
                    done();
                });
            });
        });
    });

    describe("Bicicleta.findByCode", () => {
        it("debe devolver la bici con code 1", (done) => {
            Bicicleta.allBicis((err, bicis) => {
                expect(bicis.length).toBe(0);
                let bici = new Bicicleta({
                    code: 1,
                    color: "verde",
                    modedlo: "urbana",
                });
                Bicicleta.add(bici, (err, newBici) => {
                    if (err) console.log(err);
                    let bici2 = new Bicicleta({
                        code: 2,
                        color: "roja",
                        modelo: "urbana",
                    });
                    Bicicleta.add(bici2, (err, newBici2) => {
                        if (err) console.log(err);
                        Bicicleta.findByCode(1, (error, targetBici) => {
                            expect(targetBici.code).toBe(bici.code);
                            expect(targetBici.color).toBe(bici.color);
                            expect(targetBici.modelo).toBe(bici.modelo);
                            done();
                        });
                    });
                });
            });
        });
    });

    describe("Bicicleta.removeByCode", () => {
        it("debe borrar la bici con code 1", (done) => {
            Bicicleta.allBicis((err, bicis) => {
                expect(bicis.length).toBe(0);
                let bici = new Bicicleta({
                    code: 1,
                    color: "verde",
                    modelo: "urbana",
                });
                Bicicleta.add(bici, (err, newBici) => {
                    if (err) console.log(err);
                });
                Bicicleta.removeByCode(1, (error) => {
                    console.log(error)
                    Bicicleta.findByCode(1, (error, targetBici) => {
                        console.log(targetBici)
                        expect(targetBici).toBe(undefined);
                    })
                });
            });
        });
    });
});
// beforeEach(() => {
//     Bicicleta.allBicis = [];
// })

// describe('Bicicleta.allBicis', () => {
//     it('comienza vacia', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);
//     });
// });

// describe('Bicicleta.add', () => {
//     it('agregamos una', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);
//         const bici = new Bicicleta(1, 'rojo', 'urban', [0, 0]);
//         Bicicleta.add(bici);
//         expect(Bicicleta.allBicis.length).toBe(1);
//         expect(Bicicleta.allBicis[0]).toBe(bici);
//     })
// })

// describe('Bicicleta.findById', () => {
//     it('debe devolver la bici con id 1', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);
//         const bici = new Bicicleta(1, 'rojo', 'urban', [0, 0]);
//         const bici2 = new Bicicleta(2, 'verde', 'mountain', [0, 0]);
//         Bicicleta.add(bici);
//         Bicicleta.add(bici2);

//         const targetBici = Bicicleta.findById(1);
//         expect(targetBici.id).toBe(1);
//         expect(targetBici.color).toBe(bici.color);
//         expect(targetBici.modelo).toBe(bici.modelo);
//     })
// })
