const mongoose = require('mongoose');
let Schema = mongoose.Schema;

let bicicletaSchema = new Schema({
    code: Number,
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number], index: {
            type: '2dsphere',
            sparse: true
        }
    }
});

bicicletaSchema.statics.createInstance = function(code, color, modelo, ubicacion) {
    return new this({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    });
};

bicicletaSchema.statics.allBicis = function(cb) {
    return this.find({}, cb);
};

bicicletaSchema.methods.toString = function() {
    return 'code' + this.code + ' | color: ' + this.color; 
};

bicicletaSchema.statics.add = function(bici, cb) {
    console.log(bici)
    this.create(bici, cb);
}

bicicletaSchema.statics.findByCode = function(code, cb) {
    return this.findOne({code: code}, cb);
}

bicicletaSchema.statics.removeByCode = function(code, cb) {
    return this.deleteOne({code: code}, cb);
}

module.exports = mongoose.model('Bicicleta', bicicletaSchema);

// let Bicicleta = function (id, color, modelo, ubicacion) {
//     this.id = id;
//     this.color = color;
//     this.modelo = modelo;
//     this.ubicacion = ubicacion;
// }

// Bicicleta.prototype.toString = function () {
//     return 'id' + this.id + ' | color: ' + this.color;
// }

// Bicicleta.allBicis = [];
// Bicicleta.add = function (aBici) {
//     Bicicleta.allBicis.push(aBici);
// }

// Bicicleta.findById = function(id){
//     let bici = Bicicleta.allBicis.find(bici => bici.id === id);
//     if(bici)
//         return bici;
//     else
//         throw new Error(`No existe una bicicleta con el id ${id}.`);
// }

// Bicicleta.removeById = function(id) {
//     for (let i = 0; i < Bicicleta.allBicis.length; i++){
//         if(Bicicleta.allBicis[i].id === id){
//             Bicicleta.allBicis.splice(i, 1);
//             break;
//         }
//     }
// }

// let a = new Bicicleta(1, 'rojo', 'urbana', [41.388399,2.1333953]);
// let b = new Bicicleta(2, 'blanca', 'urbana', [41.38691,2.1329553]);

// Bicicleta.add(a);
// Bicicleta.add(b);

// module.exports = Bicicleta;