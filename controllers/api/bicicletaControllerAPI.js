let Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function(req, res) {
    res.status(200).json({
        bicicletas: Bicicleta.allBicis
    });
}

exports.bicicleta_create_post = function(req, res) {
    let bici = new Bicicleta({
        code: req.body.id,
        color: req.body.color,
        modelo: req.body.modelo,
        ubicacion: req.body.ubicacion
    });
    Bicicleta.add(bici);
    res.status(200).json({
        bicicleta: bici
    });
}

exports.bicicleta_delete_post = function(req, res) {
    Bicicleta.removeById(req.body.id);
    res.status(200).json({
        bicicleta: bici
    })
}

exports.bicicleta_update_post = function(req, res) {
    let bici = Bicicleta.findById(req.body.id);
    bici.id = req.body.id;
    bici.colores = req.body.colores;
    bici.modelo = req.body.modelo;
    bici.ubicacion = req.body.ubicacion;
    res.status(200).json({
        bicicleta: bici
    })
}