const Bicicleta = require('../models/bicicleta');

exports.bicicleta_list = function(req, res) {
    res.send({
        bicis: Bicicleta.allBicis
    });
}

exports.bicicleta_create_get = function(req, res) {
    
}

exports.bicicleta_create_post = function(req, res) {
    let bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
    bici.ubicacion = req.body.ubicacion;
    Bicicleta.add(bici);
    res.send("Bicicleta creada.")
}

exports.bicicleta_delete_post = function(req, res) {
    Bicicleta.removeById(req.body.id);
    res.send("Bicicleta borrada.")
}

exports.bicicleta_update_post = function(req, res) {
    let bici = Bicicleta.findById(req.body.id);
    bici.id = req.body.id;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = req.body.ubicacion;
    res.send("Bicicleta actualizada");
}