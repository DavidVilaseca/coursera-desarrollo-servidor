let mymap = L.map('mapid').setView([41.3875376, 2.1352943], 17);
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://openstreetmap.org/copyright">OpenStreetMap contributors</a>'
}).addTo(mymap);

async function getMarkers() {
    let result = await fetch("http://localhost:3000/api/bicicletas");
    let marks = await result.json();
    marks.bicicletas.forEach(bici => {
        L.marker(bici.ubicacion, { title: bici.id }).addTo(mymap);
    });
};

getMarkers();
